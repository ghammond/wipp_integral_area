# -*- coding: utf-8 -*-
"""
Created on Thu Mar  1 11:20:37 2018

@author: gehammo
"""

import abs_integral_area

# testing  
numtimes1 = 20
numtimes2 = 31
mintime1 = 51.
mintime2 = 1.
maxtime1 = 501.
maxtime2 = 503.
times1 = arange(mintime1,maxtime1+1.,(maxtime1-mintime1)/numtimes1)
times2 = arange(mintime2,maxtime2+1.,(maxtime2-mintime2)/numtimes2)
#values1 = zeros(times1.size)
#values2 = zeros(times2.size)
values1 = random.rand(times1.size)
values2 = random.rand(times2.size)
#print(values1)
#print(values2)
#values1 = array([ 0.78725322 , 0.09174663 , 0.53409944 , 0.81227184 , 0.02095335])
#values2 = array([ 0.24404559 , 0.5626805 ,  0.72840779 , 0.619935 ,   0.6476897 ,  0.99105375])

times1 = array([0.,0.5,1.,1.5,2.,2.5,3.,3.5,4.])
values1 = array([0.,50.,100.,150.,200.,250.,300.,350.,400.])

times2 = array([0.,1.,2.,3.,4.])
#values2 = array([0.,100.,200.,300.,400.])
values2 = array([0.,50.,250.,250.,400.]) # W

# duplicate times
#times1 = array([0.,0.5,1.,1.,1.,1.5,2.,2.5,3.,3.5,4.])
#values1 = array([0.,50.,100.,100.0001,100.0002,150.,200.,250.,300.,350.,400.])

[area,times3,values3] = \
  IntegralAreaBetweenCurves(times1,values1,times2,values2,'all')
print('Area: %f' % area)
#print(times3)
#print(values3)
f = plt.figure(figsize=(6,6))
plt.plot(times1,values1,marker='+',label='v1')
plt.plot(times2,values2,marker='+',label='v2')
plt.plot(times3,values3,marker='x',label='v3')
plt.legend(loc=1)
plt.show()

# check
area = 0.
for i in range(times3.size-1):
  area += abs((values3[i+1]+values3[i])/2.*(times3[i+1]-times3[i]))
print('Check: %f' % area)
print(times3)
print(values3)