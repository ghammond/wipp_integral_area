# -*- coding: utf-8 -*-
"""
Created on Thu Feb 22 11:35:29 2018

Integrates the absolute value of the area between two lines

@author: gehammo
"""
from sys import *
from math import *
from numpy import *
import matplotlib.pyplot as plt

class Line:
  '''Based on the equation for a line y = mx + b where m is the slope and
     b is the y intercept at x = 0.
     '''
  def __init__(self,x1,y1,x2,y2):
    self.xstart = x1
    self.xend = x2
    self.ystart = y1
    self.yend = y2
    x2mx1 = x2-x1
    # avoid divide by zero
    if abs(x2mx1) > 0.:
      self.m = (y2-y1)/x2mx1
    else:
      self.m = 1.e20
    self.b = y1-self.m*x1
  def ValueYAtX(self,x):
    return self.m*x+self.b

def LineSegmentsIntersect(line1,line2,tstart,tend):
  if abs(line1.m-line2.m) > 1e-40:
    x = (line2.b-line1.b)/(line1.m-line2.m)
    if x >= tstart and x >= tstart and \
       x < tend and x < tend:
      return [True,x]
  return [False,-999.]

def NonOverlappingAreaOfNonIntersectingLines(tstart,tend,line1,line2):
  #print('Area Calc:')
  #print(line1.xstart,line1.xend,line1.ystart,line1.yend,' - > ',
  #      (line1.ystart+line1.yend)/2.)
  #print(line2.xstart,line2.xend,line2.ystart,line2.yend,' - > ',
  #      (line2.ystart+line2.yend)/2.)
  #print((line1.ystart+line1.yend)/2.-(line2.ystart+line2.yend)/2.)
  #print(tstart,tend, ' -> ',tend-tstart)
  if tend <= tstart:
    if tend < tstart:
      print('Error in NonOverlappingAreaOfNonIntersectingLines:')
      print('  end time (%f) < start time (%f)' % (tend,tstart))
      exit(0)
    return 0.
  return abs((line1.ValueYAtX(tstart)+line1.ValueYAtX(tend))-
             (line2.ValueYAtX(tstart)+line2.ValueYAtX(tend)))/2.*(tend-tstart)

def IntegralAreaBetweenCurves(times1,values1,times2,values2,difference_string):
  difference_flag = 3
  if difference_string.startswith('first') or \
     difference_string.startswith('one'):
    difference_flag = 1
  elif difference_string.startswith('second') or \
       difference_string.startswith('two'):
    difference_flag = 2
  elif difference_string.startswith('all'):
    difference_flag = 4
  # the size of the times and values arrays must be identical, but they can
  # differ between 1 and 2
  if times1.size != values1.size:
    print('Size of times1 (%d) does not match size of values1 (%d)' %
          (times1.size,values1.size))
    exit(0)
  if times2.size != values2.size:
    print('Size of times1 (%d) does not match size of values1 (%d)' %
          (times1.size,values1.size))
    exit(0)
  max_time = min(amax(times1),amax(times2))
  min_time = max(amin(times1),amin(times2))
  size1 = times1.size
  size2 = times2.size
#  times3 = zeros(max(size1,size2),dtype='f8')
#  values3 = zeros(max(size1,size2),dtype='f8')
  times3 = zeros(3,dtype='f8')
  values3 = zeros(3,dtype='f8')
  # Evaluate over segments between 0 and maximum time
  tstart = min_time
  i1 = 0
  i2 = 0
  count3 = 0
  total_area = 0.
  while tstart < max_time:
    while tstart >= times1[i1+1]:
      i1 += 1
    while tstart >= times2[i2+1]:
      i2 += 1
    if i1+1 >= size1 and i2+1 >= size2:
      break
    tstart = max(times1[i1],times2[i2])
    tend = min(times1[i1+1],times2[i2+1])
#    print('data: %f %f %f %f'%(tstart,tend,values1[i1:i1+1],values1[i2:i2+1]))
    # sum the area between time tstart and tend
    segment_area = 0.
    line1 = Line(times1[i1],values1[i1],times1[i1+1],values1[i1+1])
    line2 = Line(times2[i2],values2[i2],times2[i2+1],values2[i2+1])

    # it is possible that three values could be appended
    # therefore, we increase size of count+3 is greater than existing size
    if count3+3 >= times3.size:
      times3.resize(times3.size*2)
      values3.resize(values3.size*2)
      # zero values beyond original values
      times3[count3:times3.size] = 0.
      values3[count3:values3.size] = 0.        
    if count3 == 0:
      times3[count3] = tstart
      values3[count3] = line2.ValueYAtX(tstart) - line1.ValueYAtX(tstart)
      count3 += 1
    [they_intersect,time_of_intersection] = \
      LineSegmentsIntersect(line1,line2,tstart,tend)
    if they_intersect:
#      print('intersection @ %f between %f and %f' % 
#            (tstart,time_of_intersection,tend))
      segment1_area = \
        NonOverlappingAreaOfNonIntersectingLines(tstart,time_of_intersection,
                                                 line1,line2)
      segment_area += segment1_area
 #     print('intersection 1 area: %f %f' % 
 #           (time_of_intersection,segment1_area))
      if difference_flag > 3:
        times3[count3] = time_of_intersection
        values3[count3] = line2.ValueYAtX(time_of_intersection) - \
                          line1.ValueYAtX(time_of_intersection)
        count3 += 1
      segment2_area = \
        NonOverlappingAreaOfNonIntersectingLines(time_of_intersection,tend,
                                                 line1,line2)
      segment_area += segment2_area
#      print('intersection 2 area: %f %f' % 
#            (tend,segment2_area))
    else:
      segment_area += \
        NonOverlappingAreaOfNonIntersectingLines(tstart,tend,line1,line2)
    total_area += segment_area
 #   print(tstart,tend,' : ',total_area,segment_area)
    if difference_flag >= 3 or \
       (difference_flag == 1 and abs(tend-times1[i1+1]) < 1.e-20) or \
       (difference_flag == 2 and abs(tend-times2[i2+1]) < 1.e-20):
      times3[count3] = tend
      values3[count3] = line2.ValueYAtX(tend) - line1.ValueYAtX(tend)
      count3 += 1
    tstart = tend
  times3.resize(count3)
  values3.resize(count3)
  return total_area,times3,values3 


